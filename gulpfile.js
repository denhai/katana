var gulp = require('gulp')
var sass = require('gulp-sass')
var bs = require('browser-sync')

var browserify = require('browserify');
var gutil = require('gulp-util');
var tap = require('gulp-tap');
var buffer = require('gulp-buffer');

var paths = {
  html: 'src/**/*.html',
  styles: 'src/styles/**/*.scss',
  scripts: 'src/scripts/**/*.js',
  static: 'src/static/**/*.*'
}

gulp.task('browser-sync', ['html', 'styles', 'scripts', 'static'], function () {
  bs({
    server: {
      baseDir: './build'
    }
  })
})

gulp.task('bs-reload', function () {
  bs.reload()
})

gulp.task('styles', function () {
  return gulp.src(paths.styles)
    .pipe(sass())
    .pipe(gulp.dest('build/styles/'))
    .pipe(bs.reload({ stream: true }))
})

gulp.task('scripts', function() {
  return gulp.src(paths.scripts, {read: false})
    .pipe(tap(function (file) {
      gutil.log('bundling ' + file.path);
      file.contents = browserify(file.path, {debug: true}).bundle();
    }))
    .pipe(buffer())
    .pipe(gulp.dest('build/scripts'))
    .pipe(bs.reload({ stream: true }))
})

gulp.task('html', function() {
  return gulp.src(paths.html)
    .pipe(gulp.dest('build'))
    .pipe(bs.reload({ stream: true }))
})

gulp.task('static', function() {
    return gulp.src(paths.static)
        .pipe(gulp.dest('build'))
})

// var uncss = require('gulp-uncss');
// gulp.task('uncss', function() {
//   gulp.src('build/**/*.css')
//   .pipe(uncss({
//     html: ['build/**/*.html']
//   }))
//   .pipe(gulp.dest('build'))
// })

gulp.task('default', ['browser-sync'], function () {
  gulp.watch(paths.styles, ['styles'])
  gulp.watch(paths.scripts, ['scripts'])
  gulp.watch(paths.html, ['html'])
  gulp.watch(paths.statics, ['static'])
})